import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func config(name: String, score: Int, date: String) {
        
        nameLabel.text = ("Name:  ".localization() + name)
        scoreLabel.text = ("Score:  ".localization() + String(score))
        dateLabel.text = ("Date:  ".localization() + date)
    }
}
