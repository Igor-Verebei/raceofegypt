import Foundation

class SettingsModel: NSObject, Codable {
    
    var name: String?
    var car: String?
    var obstacle: String?
    
    init(name: String, car: String, obstacle: String) {
        self.name = name
        self.car = car
        self.obstacle = obstacle
    }
    
    public enum CodingKeys: String, CodingKey {
        case name, car, obstacle
    }
    
    public override init() {}
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.car, forKey: .car)
        try container.encode(self.obstacle,  forKey: .obstacle)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.car = try container.decodeIfPresent(String.self, forKey: .car)
        self.obstacle = try container.decodeIfPresent(String.self, forKey: .obstacle)
    }
}
