import UIKit

protocol CrashViewControllerDelegate: AnyObject {
    func checkCrash()
    func jumpCar()
    func accelControl()
}

class CrashViewController: UIViewController {
    
    @IBOutlet weak var tryButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var goLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    weak var delegate: CrashViewControllerDelegate?
    private var score = RaceManager.shared.count
    private var name = SettingsManager.shared.getSettings().name
    let date = NSDate()
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let userDate = RecordsManager.shared.dateToString(date as Date)
        guard let userName = self.name else {return}
        let userRecord = RecordsModel(name: userName, score: self.score, date: userDate)
        var recordsArray = RecordsManager.shared.getSetings()
        recordsArray.insert(userRecord, at: 0)
        RecordsManager.shared.setSettings(recordsArray)
    }
    
    private func setUI() {
        tryButton.roundCorners()
        tryButton.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        menuButton.roundCorners()
        menuButton.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        let font = UIFont(name: "mr_HouseBrokenRoughG", size: 60)
        goLabel.font = font
        nameLabel.text = name
        scoreLabel.text = ("your score:  ".localization() + String(score))
    }
    
    @IBAction func tryButton(_ sender: UIButton) {
        RaceManager.shared.count = 0
        RaceManager.shared.score()
        self.delegate?.checkCrash()
        self.delegate?.accelControl()
        self.delegate?.jumpCar()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

