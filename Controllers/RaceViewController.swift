import UIKit
import CoreMotion

class RaceViewController: UIViewController {
    
    @IBOutlet weak var obstacleView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var carPosition: NSLayoutConstraint!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var roadView: UIView!
    @IBOutlet weak var carWidth: NSLayoutConstraint!
    @IBOutlet weak var carHeight: NSLayoutConstraint!
    @IBOutlet weak var leftOutOfRoad: UIView!
    @IBOutlet weak var rightOutOfRoad: UIView!
    private var timerForRoad = Timer()
    private var timerForTips = Timer()
    private var timerForCrash = Timer()
    private var timerForJump = Timer()
    private var accelTimer = Timer()
    private var againTimer = Timer()
    private let obstacle = UIImageView()
    var motionManager = CMMotionManager()
    private var userSettings: SettingsModel?
    private var shouldCheckGyroScope = true
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkCrash()
        showTips()
        stones()
        roadLines()
        leftThrees()
        rightThrees()
        RaceManager.shared.score()
        RaceManager.shared.count = 0
        motionManager.startAccelerometerUpdates()
        accelControl()
        motionManager.startGyroUpdates()
        jumpCar()
        RaceManager.shared.delegate = self
        setUI()
    }
    
    private func showTips() {
        let tipsLabel = UILabel()
        tipsLabel.frame.size = CGSize(width: 300, height: 100)
        tipsLabel.textColor = .white
        tipsLabel.font = UIFont.systemFont(ofSize: 25)
        tipsLabel.textAlignment = .center
        tipsLabel.text = "Shake to Jump".localization()
        tipsLabel.center = view.center
        self.view.addSubview(tipsLabel)
        timerForTips = Timer.scheduledTimer(withTimeInterval: 2.5, repeats: true, block: { (_) in
            tipsLabel.removeFromSuperview()
        })
    }
    
    private func roadLines() {
        let roadWidth = self.roadView.frame.size.width
        timerForRoad = Timer.scheduledTimer(withTimeInterval: 0.3, repeats: true, block: { [weak self] (_) in
            guard let self = self else { return }
            let roadLanes = UIView()
            roadLanes.frame = CGRect(x: roadWidth/2, y: -500, width: 10, height: 120)
            roadLanes.backgroundColor = .white
            self.roadView.addSubview(roadLanes)
            UIView.animate(withDuration: 7 ) {
                roadLanes.frame.origin.y = 2000}
        })
        timerForRoad.fire()
    }
    
    private func leftThrees() {
        timerForRoad = Timer.scheduledTimer(withTimeInterval: 1.2, repeats: true, block: { [weak self] (_) in
            guard let self = self else { return }
            let leftThrees = UIImageView(image: UIImage(named: "treeImage"))
            leftThrees.frame = CGRect(x: 15, y: -200, width: 40, height: 40)
            self.view.addSubview(leftThrees)
            UIView.animate(withDuration: 15 ) {
                leftThrees.frame.origin.y = 2000}
        })
        timerForRoad.fire()
    }
    
    private func rightThrees() {
        let viewWidth = view.frame.size.width
        timerForRoad = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (_) in
            guard let self = self else { return }
            let rightThrees = UIImageView(image: UIImage(named: "treeImage"))
            rightThrees.frame = CGRect(x: viewWidth-55, y: -150, width: 40, height: 40)
            self.view.addSubview(rightThrees)
            UIView.animate(withDuration: 15 ) {
                rightThrees.frame.origin.y = 2000}
        })
    }
    
    func stones(){
        let obstacleViewWidth = self.obstacleView.frame.size.width
        timerForRoad = Timer.scheduledTimer(withTimeInterval: Costants.stoneInterval, repeats: true, block: { [weak self] (_) in
            guard let self = self else { return }
            self.obstacle.frame = CGRect(x: obstacleViewWidth/2 + CGFloat(Int.random(in: -100...50)) , y: -500, width: 80, height: 80)
            self.obstacle.contentMode = .scaleAspectFill
            self.obstacleView.addSubview(self.obstacle)
            UIView.animate(withDuration: 7) {
                self.obstacle.frame.origin.y = 2000}
        })
    }
    
    private func setUI() {
        scoreLabel.roundCorners()
        self.userSettings = SettingsManager.shared.getSettings()
        carImageView.image = UIImage(named: userSettings?.car ?? "yellowCarImage")
        obstacle.image = UIImage(named: userSettings?.obstacle ?? "stoneImage")
    }
    
    @IBAction func close(_ sender: UIButton) {
        RaceManager.shared.timerForScore.invalidate()
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension RaceViewController : CrashViewControllerDelegate, RaceManagerDelegate {
    func setScoreLabelText() {
        self.scoreLabel.text = ("Score:   ".localization() + String(RaceManager.shared.count))
    }
    
    func checkCrash() {
        timerForCrash = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (_) in
            guard let self = self else {return}
            guard let stoneFrame = self.obstacle.layer.presentation() else {return}
            if self.carImageView.frame.intersects(self.leftOutOfRoad.frame) ||
                self.carImageView.frame.intersects(self.rightOutOfRoad.frame) ||
                self.carImageView.frame.intersects(stoneFrame.frame) {
                self.timerForCrash.invalidate()
                RaceManager.shared.timerForScore.invalidate()
                self.accelTimer.invalidate()
                self.timerForJump.invalidate()
                self.view.layer.removeAllAnimations()
                self.carPosition.constant = self.roadView.frame.size.width/2
                guard let crashController = self.storyboard?.instantiateViewController(identifier: "CrashViewController") as? CrashViewController else {return}
                crashController.delegate = self
                self.navigationController?.pushViewController(crashController, animated: true)
            }
        })
        timerForCrash.fire()
    }
    
    func jumpCar() {
        timerForJump = Timer.scheduledTimer(withTimeInterval: 1/13, repeats: true) { [weak self] (_) in
            guard let self = self else {return}
            guard let jump = self.motionManager.gyroData?.rotationRate.x else {return}
            if self.shouldCheckGyroScope {
                
                if jump >= 2 {
                    
                    self.timerForCrash.invalidate()
                    self.shouldCheckGyroScope = false
                    self.carWidth.constant *= 1.5
                    self.carHeight.constant *= 1.5
                    UIImageView.animate(withDuration: 0.1) {
                        self.view.layoutIfNeeded()
                    }
                    
                    self.againTimer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { [weak self] (_) in
                        guard let self = self else {return}
                        self.carWidth.constant /= 1.5
                        self.carHeight.constant /= 1.5
                        UIImageView.animate(withDuration: 0.3) {
                            self.view.layoutIfNeeded()
                        }
                        self.shouldCheckGyroScope = true
                        self.checkCrash()
                    }
                }
            }
            
        }
    }
    
    func accelControl(){
        accelTimer = Timer.scheduledTimer(withTimeInterval: 1/60, repeats: true) { [weak self] (_) in
            guard let self = self else {return}
            guard let motion = self.motionManager.accelerometerData?.acceleration.x else {return}
            self.carPosition.constant += CGFloat(motion) * 18
            UIImageView.animate(withDuration: 0.01) {
                self.view.layoutIfNeeded()
            }
        }
        accelTimer.fire()
    }
}
