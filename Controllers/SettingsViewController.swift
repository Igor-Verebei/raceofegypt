
import UIKit

class SettingsViewController: UIViewController {
    
    private var userSettings: SettingsModel?
    @IBOutlet weak var nameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userSettings = SettingsManager.shared.getSettings()
        self.nameField.placeholder = userSettings?.name
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(true)
        
        self.userSettings?.name = nameField.text
        self.view.endEditing(true)
        if let userSettings = self.userSettings {
            SettingsManager.shared.setSettings(userSettings)
        }
    }
    
    @IBAction func clickClose(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func yellowCarButton(_ sender: UIButton) {
        self.userSettings?.car = "yellowCarImage"
    }
    
    @IBAction func blueCarButton(_ sender: UIButton) {
        self.userSettings?.car = "blueCarImage"
    }
    
    @IBAction func stoneObstacleButton(_ sender: UIButton) {
        self.userSettings?.obstacle = "stoneImage"
    }
    
    @IBAction func roadSignButton(_ sender: UIButton) {
        self.userSettings?.obstacle = "roadSignImage"
    }
}

extension SettingsViewController : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}




