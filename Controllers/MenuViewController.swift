import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var recordsButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var backView: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    fileprivate func setupUI() {
        nameLabel.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        startButton.roundCorners()
        startButton.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        recordsButton.roundCorners()
        recordsButton.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        settingsButton.roundCorners()
        settingsButton.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
    }
    
    @IBAction func clickStart(_ sender: UIButton) {
        guard let gameController = self.storyboard?.instantiateViewController(withIdentifier: "RaceViewController") as? RaceViewController else {return}
        self.navigationController?.pushViewController(gameController, animated: true)
    }
    
    @IBAction func clickRecords(_ sender: UIButton) {
        guard let recordsController = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else {return}
        recordsController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(recordsController, animated: true)
    }
    
    @IBAction func clickSettings(_ sender: UIButton) {
        guard  let settingsController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {return}
        settingsController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(settingsController, animated: true)
    }
}


