
import UIKit

class RecordsViewController: UIViewController {
    
    @IBOutlet weak var resultsTable: UITableView!
    private var results = RecordsManager.shared.getSetings()
    private var cellIdentifier = "CustomTableViewCell"
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickClose(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension RecordsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)  as? CustomTableViewCell else {
            return UITableViewCell() }
        
        cell.config(name: results[indexPath.row].name!,
                    score: results[indexPath.row].score!,
                    date: results[indexPath.row].date!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        results.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        RecordsManager.shared.setSettings(results)
        tableView.reloadData()
    }
}
