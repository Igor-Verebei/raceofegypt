import Foundation

class RecordsManager {
    
    static var shared = RecordsManager()
    var recordsArray = [RecordsModel]()
    private let defaultSettings = RecordsModel(name: "driver", score: 0, date: "date")
    private let recordsKey = "recKey"
    
    func getSetings() -> [RecordsModel] {
        if let settings = UserDefaults.standard.value([RecordsModel].self, forKey: recordsKey) {
            return settings
        }
        return []
    }
    
    func setSettings(_ settings: [RecordsModel]) {
        UserDefaults.standard.set(encodable: settings, forKey: recordsKey)
    }
    
    func dateToString(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm dd.MM.yyyy"
        return formatter.string(from: date)
    }
}
