import Foundation

class SettingsManager {
    
    static var shared = SettingsManager()
    private init(){}
    private let defaultSettings = SettingsModel(name: "driver", car: "yellowCarImage", obstacle: "stoneImage")
    private let settingsKey = "setKey"
    
    func getSettings() -> SettingsModel{
        if let settings = UserDefaults.standard.value(SettingsModel.self, forKey: settingsKey) {
            return settings
        }
        return defaultSettings
    }
    
    func setSettings(_ settings: SettingsModel){
        UserDefaults.standard.set(encodable: settings, forKey: settingsKey)
    }
}

extension UserDefaults {
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
