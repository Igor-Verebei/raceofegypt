import Foundation

protocol RaceManagerDelegate: AnyObject {
    func setScoreLabelText()
}

class RaceManager {
    
    static var shared: RaceManager = RaceManager()
    weak var delegate: RaceManagerDelegate?
    var timerForScore = Timer()
    var count: Int = 0
    
    func score () {
        timerForScore = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (_) in
            self.count += 1
            self.delegate?.setScoreLabelText()
        })
    }
    private init(){}
}

